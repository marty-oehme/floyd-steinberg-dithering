import 'dart:html';
import 'package:dart_floyd_steinberg_dithering/Dither.dart';

CanvasElement input;
CanvasElement output;

ImageElement inputImg;
ImageElement outputImg;

void main() {
  inputImg = new ImageElement(src: 'kitten.jpg');

  input = document.querySelector('#input');
  output = document.querySelector('#output');

  input.width = inputImg.width;
  input.height = inputImg.height;
  output.width = inputImg.width;
  output.height = inputImg.height;
  inputImg.onLoad.listen(imgLoaded);
}

void imgLoaded(Event e) {
  print("image loaded");
  input.context2D.drawImage(inputImg, 0, 0);

  PixelArray px = new PixelArray.fromImageData(getImageData(input), input.width);
  px = Pixed.greyscale(px);
  px = Pixed.dither(px);
  output.context2D.putImageData(px.toImageData(), 0, 0);
}

ImageData getImageData(CanvasElement canvas) {
  return canvas.context2D.getImageData(0, 0, canvas.width, canvas.height);
}